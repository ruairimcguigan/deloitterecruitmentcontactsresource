package com.deloitte.recruitment.contact.resource;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.facebook.stetho.Stetho;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.makeText;

/**
 * Created by rmcguigan on 14/03/16.
 */
public class ApplicationContextProvider extends Application {

    /**
     * Keeps a reference of the application context
     */
    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        viewSQLiteHelper();
    }

    /**
     * Returns the application context
     *
     * @return application context
     */
    public static Context getContext(){
        return mContext;
    }

    private void viewSQLiteHelper() {
        Stetho.newInitializerBuilder(this)
                .enableDumpapp(
                        Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(
                        Stetho.defaultInspectorModulesProvider(this))
                .build();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    //If needed - move tp BaseActivity Activity
    public static void makeSnackBar(View view, final String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeText(ApplicationContextProvider.getContext(),message, LENGTH_LONG).show();
            }
        }).show();
    }
}
