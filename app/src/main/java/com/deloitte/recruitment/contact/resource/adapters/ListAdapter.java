package com.deloitte.recruitment.contact.resource.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.model.Candidate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rmcguigan on 24/11/15.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.CandidateViewholder> {

    Context mContext;
    public List<Candidate> mCandidateList = new ArrayList<>();

    public ListAdapter(Context context, List<Candidate> candidates){
        mContext = context;
        mCandidateList = candidates;
    }

    @Override
    public CandidateViewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.candidate_card_item,
                viewGroup, false);
        CandidateViewholder candidateViewholder = new CandidateViewholder(view);
        return candidateViewholder;
    }

    @Override
    public void onBindViewHolder(CandidateViewholder holder, int position) {

        Candidate candidateObject = mCandidateList.get(position);

        holder.name.setText(candidateObject.getName());
        holder.phoneNumber.setText(candidateObject.getPhone());
        holder.email.setText(candidateObject.getEmail());
        holder.university.setText(candidateObject.getUniversity());
        holder.expectedGrade.setText(candidateObject.getExpectedGrade());
        holder.graduationDate.setText(candidateObject.getGraduationDate());
    }

    @Override
    public int getItemCount() {
        return mCandidateList.size();
    }

    public class CandidateViewholder extends RecyclerView.ViewHolder {

        TextView name, phoneNumber, email, university, expectedGrade, graduationDate;

        public CandidateViewholder(View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.candidate_name);
            phoneNumber = (TextView)itemView.findViewById(R.id.contact_number);
            email = (TextView)itemView.findViewById(R.id.candidate_email);
            university = (TextView)itemView.findViewById(R.id.candidate_university);
            expectedGrade = (TextView)itemView.findViewById(R.id.candidate_grade);
            graduationDate = (TextView)itemView.findViewById(R.id.graduation_date);
        }
    }


}
