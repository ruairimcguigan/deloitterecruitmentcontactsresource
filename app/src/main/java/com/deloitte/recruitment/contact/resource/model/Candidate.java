package com.deloitte.recruitment.contact.resource.model;

/**
 * Created by rmcguigan on 13/03/16.
 */
public class Candidate {

    long candidateId;
    String name, email, phone, university, course, expectedGrade, graduationDate;

    public long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(long candidateId) {
        this.candidateId = candidateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getExpectedGrade() {
        return expectedGrade;
    }

    public void setExpectedGrade(String expectedGrade) {
        this.expectedGrade = expectedGrade;
    }

    public String getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(String graduationDate) {
        this.graduationDate = graduationDate;
    }





}
