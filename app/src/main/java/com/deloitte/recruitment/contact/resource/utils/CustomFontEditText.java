package com.deloitte.recruitment.contact.resource.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.deloitte.recruitment.contact.resource.R;

/**
 * Created by evalera on 20/01/2015.
 */
public class CustomFontEditText extends EditText {

    private Typeface mType;
    private TypedArray mTypedArray;
    private Resources mResources = getResources();


    public CustomFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void init(AttributeSet attrs) {

        boolean isBold = false;

        if (attrs != null) {
            int[] textStyleAttr = new int[]{android.R.attr.textStyle};
            mTypedArray = getContext().obtainStyledAttributes(attrs, textStyleAttr);

            int textStyle = mTypedArray.getInt(0, -1);

            mTypedArray.recycle();

            if (textStyle == Typeface.BOLD) {
                isBold = true;
            }

            if (isBold) {
                mType = Typeface.createFromAsset(getContext().getAssets(), mResources.getString(R.string.font_path_bold));

            } else {
                mType = Typeface.createFromAsset(getContext().getAssets(), mResources.getString(R.string.font_path_light));
            }
            setTypeface(mType);

        }
    }
}
