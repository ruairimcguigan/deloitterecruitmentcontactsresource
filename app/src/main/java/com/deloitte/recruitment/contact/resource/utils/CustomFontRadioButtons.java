package com.deloitte.recruitment.contact.resource.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by rmcguigan on 20/03/16.
 */
public class CustomFontRadioButtons extends RadioButton {

    public CustomFontRadioButtons(Context context) {
        super(context);
    }

    public CustomFontRadioButtons(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomFontRadioButtons(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf.createFromAsset(getContext().getAssets(),"fonts/GalanoGrotesque-Light.otf"));
    }
}
