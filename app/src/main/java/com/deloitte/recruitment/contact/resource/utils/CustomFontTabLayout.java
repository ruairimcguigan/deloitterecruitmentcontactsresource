package com.deloitte.recruitment.contact.resource.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deloitte.recruitment.contact.resource.R;

/**
 * Created by rmcguigan on 13/01/16.
 */
public class CustomFontTabLayout extends TabLayout {

    private Typeface mTypeface;

    public CustomFontTabLayout(Context context) {
        super(context);
        init();
    }

    public CustomFontTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Resources res = getContext().getResources();
        mTypeface = Typeface.createFromAsset(getContext().getAssets(),  res.getString(R.string.font_path_bold));
    }

    @Override
    public void addTab(Tab tab) {
        super.addTab(tab);

        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();
        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
            if (tabViewChild instanceof TextView) {
                ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
            }
        }
    }

}
