package com.deloitte.recruitment.contact.resource.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.deloitte.recruitment.contact.resource.R;


public class CustomFontTextView extends TextView {
    private Typeface mType;
    private TypedArray mTypedArray;

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public String text()
    {
        return this.getText().toString();
    }

    public void init(AttributeSet attrs) {
        boolean isBold = false;

        if (attrs != null) {
            int[] textStyleAttr = new int[]{android.R.attr.textStyle};
            mTypedArray = getContext().obtainStyledAttributes(attrs, textStyleAttr);

            int textStyle = mTypedArray.getInt(0, -1);

            mTypedArray.recycle();

            if (textStyle == Typeface.BOLD) {
                isBold = true;
            }

            mType = getCustomTypeface(getContext(), isBold);

            setTypeface(mType);
        }
    }

    /**
     * Make Typeface available for use in other components
     *
     * @param context
     * @param isBold
     * @return the typeface for the custom font
     *
     */
    public static Typeface getCustomTypeface(Context context, boolean isBold) {

        Resources res = context.getResources();

        if (isBold) {
            return Typeface.createFromAsset(context.getAssets(), res.getString(R.string.font_path_bold));
        } else {
            return Typeface.createFromAsset(context.getAssets(), res.getString(R.string.font_path_light));
        }
    }
}
