package com.deloitte.recruitment.contact.resource.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.deloitte.recruitment.contact.resource.ApplicationContextProvider;
import com.deloitte.recruitment.contact.resource.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;

import au.com.bytecode.opencsv.CSVWriter;
import db.DBOpenHelper;

/**
 * Declared public and final - not to be sub-classed and can improve efficiency at runtime.
 * Not to be declared abstract as this would imply that the class HAS to be implemented
 */
public final class ExportShareDatabaseUtil {

    public ExportShareDatabaseUtil() {
    }

    /**
     * Take sqlite db and write to csv file
     */
    public static class exportToCSV extends AsyncTask<String, Void, Boolean> {

        private final int progressTime = 2000;
        Context context = ApplicationContextProvider.getContext();
        private final ProgressDialog progressDialog = new ProgressDialog(context);

        // Can use UI thread
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            progressDialog.setMessage("Exporting database...");
            progressDialog.show();
        }

        // In worker thread
        @Override
        protected Boolean doInBackground(String... params) {
            File exportDir = new File(Environment.getExternalStoragePublicDirectory
                    (Environment.DIRECTORY_DOWNLOADS), "Exported CSV");

            DBOpenHelper openHelper = new DBOpenHelper(context);

            if (!exportDir.exists()) {
                exportDir.mkdir();
            }
            File file = new File(exportDir, "candidate.csv");

            try {
                CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
                SQLiteDatabase sqlDB = openHelper.getWritableDatabase();
                Cursor cursorCSV = sqlDB.rawQuery("SELECT * FROM candidates", null);

                csvWriter.writeNext(cursorCSV.getColumnNames());
                while (cursorCSV.moveToNext()) {
                    String arrStr[] = {
                            cursorCSV.getString(0),
                            cursorCSV.getString(1),
                            cursorCSV.getString(2),
                            cursorCSV.getString(3),
                            cursorCSV.getString(4)
                    };
                    csvWriter.writeNext(arrStr);
                }
                csvWriter.close();
                cursorCSV.close();
                return true;
            } catch (SQLException sqlEx) {
                Log.e("CandidateListActivity", sqlEx.getMessage(), sqlEx);
                return false;

            } catch (IOException e) {
                Log.e("CandidateListActivity", e.getMessage(), e);
                return false;
            }
        }

        // Interface with Ui again
        @Override
        protected void onPostExecute(Boolean success) {

            if (progressDialog.isShowing()) {
                if (success) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Export successful", Toast.LENGTH_SHORT).show();
                        }
                    }, progressTime);
                }
            } else {
                Toast.makeText(context, "Export failed - please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Facilitates exporting a copy of sqlite db to external storage
     *
     * @throws IOException
     */
    public static void writeToExternalStorage() throws IOException {
        File backupDB = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_DOWNLOADS), "candidate_db_backup.db");
        File currentDB = ApplicationContextProvider.getContext().getDatabasePath(DBOpenHelper.DATABASE_NAME);
        if (currentDB.exists()) {
            FileChannel src = new FileInputStream(currentDB).getChannel();
            FileChannel dst = new FileOutputStream(backupDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
        }
    }

    /**
     * Starts system Easy share action by passing intent.Action to startActivty
     */

    public static void attachAndSendExportedDB(Activity candidateList) {
        String filename = "candidates.csv";
        File fileLocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);
        Uri path = Uri.fromFile(fileLocation);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {candidateList.getString(R.string.export_recipient_email)}; //--> if specific recipient address needed
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        // the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, path);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        candidateList.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

}
