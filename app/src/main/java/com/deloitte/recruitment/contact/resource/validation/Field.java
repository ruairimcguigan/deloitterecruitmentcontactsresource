package com.deloitte.recruitment.contact.resource.validation;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class Field {

    List<Validation> mValidations = new ArrayList<>();
    EditText mTextField;

    private Field(EditText text){
        mTextField = text;
    }

    public static Field using(EditText editText){
        return new Field(editText);
    }

    public Field validate(Validation type){
        mValidations.add(type);
        return this;
    }

    public EditText getTextField(){
        return mTextField;
    }

    public boolean isValid() throws FieldValidationException {
        for (Validation validation : mValidations) {

            if (!validation.isValid(mTextField.getText().toString())) {
                String errorMessage = validation.getErrorMessage();
                getTextField().setError(validation.getErrorMessage());
                throw new FieldValidationException(errorMessage, mTextField);
            }
        }
        return true;
    }

}