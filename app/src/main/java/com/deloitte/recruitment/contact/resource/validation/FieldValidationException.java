package com.deloitte.recruitment.contact.resource.validation;

import android.widget.EditText;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class FieldValidationException extends Exception {
    private EditText mTextView;

    public FieldValidationException(String message, EditText editText){
        super(message);
        mTextView = editText;
    }

    public EditText getTextView(){
        return mTextView;
    }
}
