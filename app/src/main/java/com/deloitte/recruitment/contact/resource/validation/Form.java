package com.deloitte.recruitment.contact.resource.validation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import com.deloitte.recruitment.contact.resource.utils.FormUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class Form {

    private List<Field> mFields = new ArrayList<>();

    private Fragment mFragment;
    private Activity mActivity;

    public Form (Activity activity){
        mActivity = activity;
    }

    public Form (Fragment fragment){
        mFragment = fragment;
    }

    public void addField(Field field){
        mFields.add(field);
    }

    public boolean isValid(){
        boolean result = true;

        try{
            for (Field field : mFields){
                result &= field.isValid();
            }
        }catch (FieldValidationException e){
            result= false;

            EditText textField = e.getTextView();
            textField.requestFocus();
            textField.selectAll();

            FormUtils.showKeyboard(mActivity, textField);

            showErrorMessage(e.getMessage());
        }
        return result;
    }

    private void showErrorMessage(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
    }
}
