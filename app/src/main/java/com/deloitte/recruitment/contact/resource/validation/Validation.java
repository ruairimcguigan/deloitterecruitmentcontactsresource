package com.deloitte.recruitment.contact.resource.validation;

/**
 * Created by rmcguigan on 19/03/16.
 */
public interface Validation {

    String getErrorMessage();

    boolean isValid(String text);

}
