package com.deloitte.recruitment.contact.resource.validation.validators;

import android.content.Context;

import com.deloitte.recruitment.contact.resource.validation.Validation;

/**
 * Created by rmcguigan on 19/03/16.
 */
public abstract class BaseValidator implements Validation {

    protected Context mContext;

    protected BaseValidator(Context context) {
        mContext = context;
    }

    @Override
    public String getErrorMessage() {
        return null;
    }

    @Override
    public boolean isValid(String text) {
        return false;
    }
}
