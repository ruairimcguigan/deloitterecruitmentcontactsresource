package com.deloitte.recruitment.contact.resource.validation.validators;

import android.content.Context;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.validation.Validation;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class atLeastThreeCharacters extends BaseValidator {

    private final static String MIN_LENGTH_PATTERN = "/(.*[a-z]){3}/i";

    protected atLeastThreeCharacters(Context context) {
        super(context);
    }

    public static Validation build(Context context){
        return new atLeastThreeCharacters(context);
    }

    @Override
    public String getErrorMessage() {
        return mContext.getString(R.string.min_length_error);
    }

    @Override
    public boolean isValid(String text) {
        return text.matches(MIN_LENGTH_PATTERN);
    }
}
