package com.deloitte.recruitment.contact.resource.validation.validators;

import android.content.Context;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.validation.Validation;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class isEmail extends BaseValidator {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    protected isEmail(Context context) {
        super(context);
    }

    public static Validation build(Context context){
        return new isEmail(context);
    }

    @Override
    public String getErrorMessage() {
        return mContext.getString(R.string.not_valid_email);
    }

    @Override
    public boolean isValid(String text) {
        return text.matches(EMAIL_PATTERN);
    }
}
