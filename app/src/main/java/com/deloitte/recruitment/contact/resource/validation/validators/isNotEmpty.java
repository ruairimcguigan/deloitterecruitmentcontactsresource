package com.deloitte.recruitment.contact.resource.validation.validators;

import android.content.Context;
import android.text.TextUtils;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.validation.Validation;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class isNotEmpty extends BaseValidator {

    public static Validation build(Context context){
        return new isNotEmpty(context);
    }

    protected isNotEmpty(Context context) {
        super(context);
    }

    @Override
    public String getErrorMessage() {
        return mContext.getString(R.string.must_not_be_empty);
    }

    @Override
    public boolean isValid(String text) {
        return !TextUtils.isEmpty(text);
    }
}
