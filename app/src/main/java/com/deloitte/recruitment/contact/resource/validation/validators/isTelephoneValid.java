package com.deloitte.recruitment.contact.resource.validation.validators;

import android.content.Context;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.validation.Validation;

/**
 * Created by rmcguigan on 19/03/16.
 */
public class isTelephoneValid extends BaseValidator{

    private static final String UK_NUMBER_PATTERN
            = "^(?:0?(?:(\\+|[00])44)?[2-3,7-9]\\d{9})$";

    protected isTelephoneValid(Context context) {
        super(context);
    }

    public static Validation build(Context context){
        return new isTelephoneValid(context);
    }

    @Override
    public boolean isValid(String text) {
        return text.matches(UK_NUMBER_PATTERN);
    }

    @Override
    public String getErrorMessage() {
        return mContext.getString(R.string.not_valid_uk_phone);
    }
}
