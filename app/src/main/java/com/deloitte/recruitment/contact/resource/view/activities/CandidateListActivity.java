package com.deloitte.recruitment.contact.resource.view.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.adapters.ListAdapter;
import com.deloitte.recruitment.contact.resource.model.Candidate;
import com.deloitte.recruitment.contact.resource.utils.ExportShareDatabaseUtil;

import java.util.List;

import db.CandidateDataSource;
import db.DBOpenHelper;

public class CandidateListActivity extends AppCompatActivity{

    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    RecyclerView mRecyclerView;
    ListAdapter mCandidateAdapter;
    DBOpenHelper dbOpenHelper;
    CandidateDataSource candidateDataSource;

    private List<Candidate> candidateList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.candidate_list);

        setUpToolbar();
        initDrawerLayout();
        initNavigationView();
        dbOpenHelper = DBOpenHelper.getInstance(getApplicationContext());
        candidateDataSource = new CandidateDataSource(this);
        initRecyclerview();

    }

    private void initRecyclerview() {
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCandidateAdapter = new ListAdapter(this, dbOpenHelper.findAllCandidates());
        mRecyclerView.setAdapter(mCandidateAdapter);

    }


    private void initDrawerLayout() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    /**
     *  sets a listener on the navigation view so that when a drawer menu item is selected,
     *  the menu item is set to checked (this will only affect the menu items marked as checkable),
     *  The drawer is closed and a Snackbar is shown displaying the title of the selected menu item.
     */
    private void initNavigationView() {
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    private void setUpToolbar() {
        mToolbar = ( Toolbar ) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if ( mToolbar != null ) {
            ActionBar actionBar = getSupportActionBar();
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu resource file; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
              case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_settings:
                return true;
            case R.id.menu_item_share:
                try{
                    new ExportShareDatabaseUtil.exportToCSV().execute("");
                }
                catch (Exception ex){
                    Log.e(getString(R.string.error_exporting_to_csv),ex.toString());
                }
                ExportShareDatabaseUtil.attachAndSendExportedDB(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
