package com.deloitte.recruitment.contact.resource.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.utils.AndroidDatabaseManager;
import com.deloitte.recruitment.contact.resource.view.fragments.CaptureDetailsFragment;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, CaptureDetailsFragment.detailsDialogListener{

    Toolbar mToolbar;
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    DrawerLayout mDrawerLayout;
    Button mStartDetailsCapture;
    NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_landing);
        super.onCreate(savedInstanceState);
        setUpToolbar();
        setUpCollapsingToolbar();
        setUpNavigationDrawer();

//        getDetailsCaptureFragment();

        //TODO: To replace with image
        mStartDetailsCapture = (Button)findViewById(R.id.start_details_capture_button);
        mStartDetailsCapture.setOnClickListener(this);


    }

    private void setUpNavigationDrawer() {
        //Initializing NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.navigation);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                mDrawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {

                    case R.id.addCandidate:
                        Toast.makeText(getApplicationContext(), "Add candidates Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.listCandidates:
                        Intent intent = new Intent(getApplicationContext(), CandidateListActivity.class);
                        startActivity(intent);

                        return true;
                    case R.id.ExportCandidates:
                        Toast.makeText(getApplicationContext(), "Export candidates Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.virtualTour:
                        Toast.makeText(getApplicationContext(), "Virtual tour Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.settings:
                        Toast.makeText(getApplicationContext(), "Settings Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.view_sql_database:
                        Intent sqlIntent = new Intent(getApplicationContext(), AndroidDatabaseManager.class);
                        startActivity(sqlIntent);
                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });
    }

    private void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.add_candidate_toolbar);
        setSupportActionBar(mToolbar);
        if (mToolbar != null) {
            ActionBar actionBar = getSupportActionBar();
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.d_logo);
        }
    }

    private void setUpCollapsingToolbar() {
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        //mCollapsingToolbarLayout.setTitleEnabled(false);
    }



    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.start_details_capture_button){
            getDetailsCaptureFragment();
        }

    }

    private void getDetailsCaptureFragment() {

        // Using Fragment manager to add captureDetailsFragment instance to containing activity's UI
        FragmentManager fm = getSupportFragmentManager();
        CaptureDetailsFragment captureDetailsFragment = new CaptureDetailsFragment();
        /**
         *  The show method uses the FragmentManager to add the DialogFragment to the Activity
         *  using the String specified as the tag.
         *  Also worth noting that the show() method doesn't add the DialogFragment to the backstack.
         */
        captureDetailsFragment.show(fm, "capture_details_fragment"); // --> Or could use (getSupportFragmentManager, "fragment_layout")

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }


    @Override
    public void onDone(boolean state) {

        if (state) {
            Toast.makeText(MainActivity.this, "Candidate profile successfully stored", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

        }
    }




