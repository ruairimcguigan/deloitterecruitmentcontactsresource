package com.deloitte.recruitment.contact.resource.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.deloitte.recruitment.contact.resource.R;
import com.deloitte.recruitment.contact.resource.adapters.CustomSpinnerAdapter;
import com.deloitte.recruitment.contact.resource.model.Candidate;
import com.deloitte.recruitment.contact.resource.utils.CustomFontButton;
import com.deloitte.recruitment.contact.resource.utils.ExportShareDatabaseUtil;
import com.deloitte.recruitment.contact.resource.validation.Field;
import com.deloitte.recruitment.contact.resource.validation.Form;
import com.deloitte.recruitment.contact.resource.validation.validators.isEmail;
import com.deloitte.recruitment.contact.resource.validation.validators.isNotEmpty;
import com.deloitte.recruitment.contact.resource.validation.validators.isTelephoneValid;

import java.io.IOException;

import db.CandidateDataSource;

public class CaptureDetailsFragment extends DialogFragment implements View.OnClickListener {

    public static final String LOGTAG = "CANDIDATE_DETAILS_FRAGMENT";

    EditText editName, editTelephone, editEmail, editUniversity, editCourse, editPredictedGrade;
    String  mSelectedGradMonth, mSelectedGradYear;
    Spinner mSpinnerGraduateMonth, mSpinnerGraduateYear;
    CustomFontButton mDetailsSubmit, mClearDetails;
    protected ExportShareDatabaseUtil exportShareDatabaseUtil;
    Context mContext;

    CandidateDataSource candidateDataSource;
    protected View source;

    Form mForm;
    private detailsDialogListener mListener;

    String[] monthArray, yearArray;

    // Interface to check if all fields have been completed and candidate object successfully stored
    public interface detailsDialogListener{
        void onDone(boolean state);
    }

    public CaptureDetailsFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(STYLE_NO_FRAME, R.style.CustomDialog);
        mContext = getActivity();
//        exportShareDatabaseUtil = new ExportShareDatabaseUtil();
        candidateDataSource = new CandidateDataSource(getActivity());
        candidateDataSource.open();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (getDialog() != null)
            return;

        int dialogWidth = getResources().getDimensionPixelSize(R.dimen.dialog_width);
        int dialogHeight = getResources().getDimensionPixelSize(R.dimen.dialog_height);

        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Put your dialog layout in R.layout.view_confirm_box
        View view = inflater.inflate(R.layout.candidate_details_layout, container, false);

        monthArray = getContext().getResources().getStringArray(R.array.grad_months);
        yearArray = getContext().getResources().getStringArray(R.array.grad_years);

        initViews(view);
        initDateSpinner(view);
        initValidationForm();
        setClickListeners();
        setDialogPosition();

        return view;
    }

    private void initDateSpinner(View view) {

        CustomSpinnerAdapter<String> monthsAdapter =
                new CustomSpinnerAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                        getResources().getStringArray(R.array.grad_months));

        CustomSpinnerAdapter<String> yearsAdapter =
                new CustomSpinnerAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                        getResources().getStringArray(R.array.grad_years));

        mSpinnerGraduateMonth.setAdapter(monthsAdapter);
        mSpinnerGraduateYear.setAdapter(yearsAdapter);


        mSpinnerGraduateMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedGradMonth = mSpinnerGraduateMonth.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerGraduateYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedGradYear = mSpinnerGraduateMonth.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initValidationForm() {

        mForm = new Form(getActivity());
        mForm.addField(Field.using(editName).validate(isNotEmpty.build(getActivity())));
        mForm.addField(Field.using(editTelephone).validate(isNotEmpty.build(getActivity())).validate(isTelephoneValid.build(getActivity())));
        mForm.addField(Field.using(editEmail).validate(isNotEmpty.build(getActivity())).validate(isEmail.build(getActivity())));
        mForm.addField(Field.using(editUniversity).validate(isNotEmpty.build(getActivity())));


        mForm.addField(Field.using(editPredictedGrade).validate(isNotEmpty.build(getActivity())));
    }

    private void initViews(View view) {
        editName = (EditText) view.findViewById(R.id.candidate_name_edit);
        editTelephone = (EditText) view.findViewById(R.id.candidate_telephone);
        editEmail = (EditText) view.findViewById(R.id.candidate_email_edit);
        editUniversity = (EditText) view.findViewById(R.id.candidate_university_edit);
        editPredictedGrade = (EditText) view.findViewById(R.id.candidate_predicted_grade_edit);
        mSpinnerGraduateMonth = (Spinner) view.findViewById(R.id.grad_month_spinner);
        mSpinnerGraduateYear = (Spinner) view.findViewById(R.id.grad_year_spinner);

        mDetailsSubmit = (CustomFontButton) view.findViewById(R.id.submit_details_button);
        mClearDetails = (CustomFontButton) view.findViewById(R.id.clear_details_button);
    }

    private void setClickListeners() {
        mDetailsSubmit.setOnClickListener(this);
        mClearDetails.setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mListener = (detailsDialogListener) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + " must implement detailsDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        candidateDataSource.close();
    }

    /**
     * Try to position this dialog next to "source" view
     */
    private void setDialogPosition() {
        if (source == null) {
            return; // Leave the dialog in default position
        }

        /**
         * Find out location of source component on screen
         */
        int[] location = new int[2];
        source.getLocationOnScreen(location);
        int sourceX = location[0];
        int sourceY = location[1];


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_details_button:
                clearFields();
                break;
            case R.id.submit_details_button:
                validateEntry();
                try {
                    exportShareDatabaseUtil.writeToExternalStorage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void validateEntry() {
        if(mForm.isValid()){
            storeCandidateDetails();
            Toast.makeText(getActivity(), "All good", Toast.LENGTH_LONG).show();
            onSubmitCandidateDetails();
        }
    }

    private void clearFields() {
        editName.setText("");
        editTelephone.setText("");
        editEmail.setText("");
        editCourse.setText("");
        editPredictedGrade.setText("");
        editUniversity.setText("");
    }


    private void storeCandidateDetails() {

        Candidate candidateObject = new Candidate();

        candidateObject.setName(editName.getText().toString());
        candidateObject.setPhone(editTelephone.getText().toString());
        candidateObject.setEmail(editEmail.getText().toString());
        candidateObject.setUniversity(editUniversity.getText().toString());
//        candidateObject.setCourse(editCourse.getText().toString());
        candidateObject.setExpectedGrade(editPredictedGrade.getText().toString());
//      candidateObject.setGraduationDate(editGraduateDate.getText().toString());

        candidateObject = candidateDataSource.create(candidateObject);
        Log.i(LOGTAG, "Candidate created with id " + candidateObject.getCandidateId());

    }

    private void onSubmitCandidateDetails() {
        boolean mDialogStatus = true;
            mListener.onDone(mDialogStatus);
            candidateDataSource.close();
            dismiss();
    }

//    public void writeToExternalStorage() throws IOException {
//        File backupDB = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "my_data_backup.db");
//        File currentDB = getActivity().getApplicationContext().getDatabasePath(DBOpenHelper.DATABASE_NAME);
//        if (currentDB.exists()){
//            FileChannel src = new FileInputStream(currentDB).getChannel();
//            FileChannel dst = new FileOutputStream(backupDB).getChannel();
//            dst.transferFrom(src, 0, src.size());
//            src.close();
//            dst.close();
//        }
//    }

}