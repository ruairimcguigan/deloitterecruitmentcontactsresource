package db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.deloitte.recruitment.contact.resource.model.Candidate;

/**
 * Created by rmcguigan on 13/03/16.
 */
public class CandidateDataSource {

    private static final String LOGTAG = "DATASOURCE";

    SQLiteOpenHelper dbOpenHelper;
    SQLiteDatabase database;

    // Constructor takes context - instaniates SQLiteOpenHelper class
    public CandidateDataSource(Context context){
        dbOpenHelper = new DBOpenHelper(context);
    }

    /**
     * Array of strings that identifies all the columns of the table
     */
    private static final String[] allColumns =
            {
                    DBOpenHelper.CANDIDATE_ID,
                    DBOpenHelper.CANDIDATE_NAME,
                    DBOpenHelper.CANDIDATE_EMAIL,
                    DBOpenHelper.CANDIDATE_EMAIL,
                    DBOpenHelper.CANDIDATE_PHONE,
                    DBOpenHelper.CANDIDATE_UNIVERSITY,
                    DBOpenHelper.CANDIDATE_EXPECTED_GRADE,
                    DBOpenHelper.CANDIDATE_GRADUATION_DATE
//                    DBOpenHelper.CANDIDATE_CREATED
            };

    // Create DB
    public void open(){
        Log.i(LOGTAG, "Database opened");
        database = dbOpenHelper.getWritableDatabase();
    }

    // Create DB
    public void close(){
        Log.i(LOGTAG, "Database closed");
        dbOpenHelper.close();
    }

    public Candidate create(Candidate candidate){
        //Package your values into an instance of a class called contentValues - well formed
        // sql statement minus the work

        ContentValues values = new ContentValues();

        values.put(DBOpenHelper.CANDIDATE_NAME, candidate.getName());
        values.put(DBOpenHelper.CANDIDATE_EMAIL, candidate.getEmail());
        values.put(DBOpenHelper.CANDIDATE_PHONE, candidate.getPhone());
        values.put(DBOpenHelper.CANDIDATE_UNIVERSITY, candidate.getUniversity());
        values.put(DBOpenHelper.CANDIDATE_EXPECTED_GRADE, candidate.getExpectedGrade());
        values.put(DBOpenHelper.CANDIDATE_GRADUATION_DATE, candidate.getGraduationDate());

        long insertID = database.insert(DBOpenHelper.TABLE_CANDIDATES, null, values);
        candidate.setCandidateId(insertID);

        return candidate;
    }



}
