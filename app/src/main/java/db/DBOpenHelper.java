package db;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.deloitte.recruitment.contact.resource.model.Candidate;

import java.util.ArrayList;
import java.util.List;

/**
 * Define database and manage connections to it
 */
public class DBOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "DbHelper";

    private static DBOpenHelper mDbOpenHelper;

    private static final String LOGTAG = "SQLHelper";

    //Constants for db name and version
    public static final String DATABASE_NAME = "candidates.db";
    public static final int DATABASE_VERSION = 2;

    //Constants for identifying table and columns
    public static final String TABLE_CANDIDATES = "candidates";
    public static final String CANDIDATE_ID = "id";
    public static final String CANDIDATE_NAME = "candidateName";
    public static final String CANDIDATE_PHONE = "candidatePhone";
    public static final String CANDIDATE_CREATED = "timeStamp";
    public static final String CANDIDATE_EMAIL = "candidateEmail";
    public static final String CANDIDATE_UNIVERSITY = "candidateUni";
    public static final String CANDIDATE_EXPECTED_GRADE = "candidateExpectedGrade";
    public static final String CANDIDATE_GRADUATION_DATE = "candidateGradDate";

    //SQL to create table
    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_CANDIDATES + " (" +
                    CANDIDATE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    CANDIDATE_NAME + " TEXT, " +
                    CANDIDATE_PHONE + " TEXT, " +
                    CANDIDATE_EMAIL + " TEXT, " +
                    CANDIDATE_UNIVERSITY + " TEXT, " +
                    CANDIDATE_EXPECTED_GRADE + " TEXT, " +
                    CANDIDATE_GRADUATION_DATE + " TEXT" +
                    CANDIDATE_CREATED + " TEXT default CURRENT_TIMESTAMP" +
                    ")";

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBOpenHelper getInstance(Context context) {
        // Using the application context, which will ensure that an Activity's context is
        // not accidentally leaked
        if (mDbOpenHelper == null) {
            mDbOpenHelper = new DBOpenHelper(context.getApplicationContext());
        }
        return mDbOpenHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_CREATE);
        Log.i(LOGTAG, "Table has been created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CANDIDATES);
        onCreate(db);
    }

    public List<Candidate> findAllCandidates() {

        List<Candidate> candidateList = new ArrayList<>();

        //Query DB - Reference to the data that is returned by the query
//        Cursor cursor = database.query(DBOpenHelper.TABLE_CANDIDATES, allColumns, null, null, null, null, null);

        String USER_DETAIL_SELECT_QUERY = "SELECT * FROM " + TABLE_CANDIDATES;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(USER_DETAIL_SELECT_QUERY, null);

        Log.i(LOGTAG, "Returned " + cursor.getCount() + " rows");

        try {
            if (cursor.moveToFirst()) {
                // if so - traverse the dataset
                do {
                    //Get instance of data model object
                    Candidate candidate = new Candidate();
                    //Set the new instance fields with appropriate data
                    candidate.setCandidateId(cursor.getLong(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_ID)));
                    candidate.setName(cursor.getString(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_NAME)));
                    candidate.setEmail(cursor.getString(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_EMAIL)));
                    candidate.setPhone(cursor.getString(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_PHONE)));
                    candidate.setUniversity(cursor.getString(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_UNIVERSITY)));
                    candidate.setExpectedGrade(cursor.getString(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_EXPECTED_GRADE)));
                    candidate.setGraduationDate(cursor.getString(cursor.getColumnIndex(DBOpenHelper.CANDIDATE_GRADUATION_DATE)));

                    candidateList.add(candidate);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return candidateList;
    }

    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"mesage"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }


    }
}
